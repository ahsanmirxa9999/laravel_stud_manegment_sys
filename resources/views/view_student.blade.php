<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Student Management System</title>

      <!-- Bootstrap CSS -->
     <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
     <!-- Font-awesome -->
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">

     <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css" rel="stylesheet">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>


        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

    </head>
    <body>
        <div class="container" style="margin-top: 50px;">
            <h3 class="text-center text-danger"><b>Student Management System</b> </h3>
            <a href="{{ url('/') }}" class="btn btn-success mb-2">Add Student</a>

            <table class="table">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Class Name</th>
                    <th>Teacher Name</th>
                    <th>Subject Name</th>
                    <th>Name</th>
                    <th>Last Name</th>
                    <th>Address</th>
                    <th>Phone Number</th>
                    <th>Birthday</th>
                    <th>Gender</th>
                    <th>Email</th>
                    <th>Update</th>
                    <th>Delete</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($data as $key=>$value)
                    <tr>
                       <th scope="row">{{$key+1}}</th>
                       <td>{{$value->class->class_name}}</td>
                       <td>{{$value->teacher->teacher_name}}</td>
                       <td>{{$value->subject->subject_name}}</td>
                       <td>{{$value->name}}</td>
                       <td>{{$value->lname}}</td>
                       <td>{{$value->address}}</td>
                       <td>{{$value->phone}}</td>
                       <td>{{$value->birthday}}</td>
                       <td>{{$value->gender}}</td>
                       <td>{{$value->email}}</td>
                       <td><a href="{{ url('/view/edit',$value->id) }}" class="btn btn-success">Update</a></td>
                       <td><a href="{{ url('delete',$value->id) }}" class="btn btn-danger">Delete</a></td>
                    </tr>
                   @endforeach
                </tbody>
              </table>
        </div>
    </body>
</html>

<script>
    // ============== Delete Toastr ==================
  @if(Session::has('danger'))
      toastr.options = {
          "closeButton":true,
          "progressBar":true
      }
      toastr.error("{{ session('danger') }}",'error!',{timeOut:2000});
  @endif

    // ============== Update toastr =============
  @if(Session::has('success'))
      toastr.options = {
          "closeButton":true,
          "progressBar":true
      }
      toastr.success("{{ session('success') }}",'success!',{timeOut:2000});
  @endif
</script>
