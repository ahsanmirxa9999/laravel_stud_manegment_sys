<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\student_class;
use App\Models\subject;
use App\Models\teacher;
class student extends Model
{
    use HasFactory;
    protected $table = 'student';
    protected $primarykey = 'id';
    protected $guarded = [];

    function class(){
        return $this->belongsTo(student_class::class,'class_id','class_id');
    }

    public function subject(){
        return $this->belongsTo(subject::class,'subject_id','subject_id');
    }

    public function teacher(){
        return $this->belongsTo(teacher::class,'teacher_id','teacher_id');
    }
}
