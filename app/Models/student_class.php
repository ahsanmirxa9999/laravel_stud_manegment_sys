<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class student_class extends Model
{
    use HasFactory;
    protected $table = 'class';
    protected $primarykey = 'class_id';
    protected $guarded = [];
}
