<?php

namespace App\Http\Controllers;
use App\Models\student;
use App\Models\student_class;
use Illuminate\Http\Request;

class StudentController extends Controller
{

    // ================ Insert Query ===================

    public function index(){
        return view('student');
    }

    public function add_data(request $request){
        $data = new Student;
        $data->class_id=$request->input('class_id');
        $data->teacher_id=$request->input('teacher_id');
        $data->subject_id=$request->input('subject_id');
        $data->name=$request->input('name');
        $data->lname=$request->input('lname');
        $data->address=$request->input('address');
        $data->phone=$request->input('phone');
        $data->birthday=$request->input('birthday');
        $data->gender=$request->input('gender');
        $data->email=$request->input('email');
        $data->save();
        return redirect()->back()->with('success','Student has benn Add Successfully');
    }

    // ================ View Query ===================

    public function view_data(){
        $data= Student::with(['class','subject','teacher'])->get();
        return view('/view_student')->with(compact('data'));
    }

    // ============= Delete Query ==================

    public function delete($id){
        $data = student::find($id);
        $data->delete();
        return redirect()->back()->with('danger', 'Student have been deleted');
    }

    // ============ Update Query ==================

    public function edit($id){
        $data = student::with(['class','subject','teacher'])->find($id);
        return view('/update')->with(compact('data'));
    }

    public function update(Request $request, $id){
        $data = student::where('id',$id)->first();
        $data->class_id=$request['class_id'];
        $data->teacher_id=$request['teacher_id'];
        $data->subject_id=$request['subject_id'];
        $data->name=$request['name'];
        $data->lname=$request['lname'];
        $data->address=$request['address'];
        $data->phone=$request['phone'];
        $data->birthday=$request['birthday'];
        $data->gender=$request['gender'];
        $data->email=$request['email'];
        $data->save();
        return redirect('/view_student')->with('success','Student has benn Updated Successfully');
    }

}
