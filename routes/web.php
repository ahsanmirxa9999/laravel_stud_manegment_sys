<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StudentController;

Route::get('/',[StudentController::class,'index']);
Route::post('/add_data',[StudentController::class,'add_data']);
Route::get('/view_student',[StudentController::class,'view_data']);
Route::get('/delete/{id}',[StudentController::class,'delete']);
Route::get('/view/edit/{id}',[StudentController::class,'edit']);
Route::post('/update/{id}',[StudentController::class,'update']);
